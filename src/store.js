import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    selectedItems: [],
    items: [
      {
        id: 1,
        title: 'Apple iPhone XS 512GB Silver',
        image: 'https://i2.rozetka.ua/goods/7807099/56285316_images_7807099950.jpg',
        description: 'Phone XS – революционное решение в каждом дюйме. Дисплей Super Retina в двух размерах, один из которых стал самым большим в истории iPhone. Ещё более быстрый Face ID.',
        price: 58990
      },
      {
        id: 2,
        title: 'Samsung Galaxy S9 64GB Midnight Black',
        image: 'https://i1.rozetka.ua/goods/3249371/samsung_galaxy_s9_64gb_black_images_3249371727.jpg',
        description: 'Доработан и улучшен сканер отпечатков пальцев – он стал значительно быстрее и точнее.Теперь камеры и сканер отпечатков расположены вертикально, по длине корпуса и строго по центральной оси смартфона, что облегчает их тактильный поиск и взаимодействие со смартфоном в целом.',
        price: 23999
      },
      {
        id: 3,
        title: 'Google Pixel 3 Xl 4/128GB Just Black',
        image: 'https://i2.rozetka.ua/goods/10357910/69103832_images_10357910960.jpg',
        description: 'Google Pixel 3 XL – старшая версия флагмана 2018 года от компании Google, получившая 6,3-дюймовый экран и практически полностью безрамочное исполнение.',
        price: 33999
      },
      {
        id: 4,
        title: 'Huawei P20 Pro 6/128GB Black',
        image: 'https://i2.rozetka.ua/goods/4627071/huawei_p20_pro_6_128gb_black_images_4627071384.jpg',
        description: 'P20 Pro - наиболее оснащенная версия в топовой линейке P20 от компании HUAWEI. Главными отличиями модели Pro от обычной P20 является наличие трех основных камер и увеличенная до 6,1 дюйма диагональ дисплея.',
        price: 22999
      },
      {
        id: 5,
        title: 'Google Pixel 2 XL 64GB Just Black',
        image: 'https://i2.rozetka.ua/goods/7992634/57486699_images_7992634839.jpg',
        description: 'Google Pixel 2 XL - это флагманское решение 2017 года от разработчика всем известной поисковой системы. Сердцем устройства был выбран бескомпромиссный Qualcomm Snapdragon 835. Оперативной памяти установлено 4 ГБ.',
        price: 20189
      },
      {
        id: 6,
        title: 'OnePlus 6 8/256GB Midnight Black',
        image: 'https://i1.rozetka.ua/goods/7991952/57445563_images_7991952687.jpg',
        description: 'OnePlus 6 с 8 ГБ оперативной памяти, традиционно для продукции компании оснащен самой мощной начинкой на момент выхода, но при этом стоит меньше флагманов других производителей. По сравнению с моделью OnePlus 5T, одним из главных изменений модели 6 является увеличенный до 6,28 дюймов дисплей.',
        price: 17993
      }
    ]
  },
  mutations: {
    initialiseSelectedItems (state) {
      let items = localStorage.getItem('selectedItems')
      if (items) {
        state.selectedItems = JSON.parse(items)
      }
    },
    addSelectedItem (state, id) {
      let key = 1
      let lastObject = state.selectedItems.length ? state.selectedItems[0] : null
      if (lastObject && lastObject.key) {
        key += lastObject.key
      }
      let temp = state.items.filter((item) => {
        return item.id === id
      })[0]
      let item = {
        ...temp,
        key: key
      }
      state.selectedItems.unshift(item)
      localStorage.setItem('selectedItems', JSON.stringify(state.selectedItems))
    },
    removeItemFromSelectedItems (state, index) {
      state.selectedItems.splice(index, 1)
      localStorage.setItem('selectedItems', JSON.stringify(state.selectedItems))
    },
    clearSelectedItems (state) {
      state.selectedItems = []
      localStorage.setItem('selectedItems', JSON.stringify(state.selectedItems))
    }
  },
  actions: {

  }
})
